<?php

/**
 * @file(features_add_component.drush.inc)
 */

/**
 * Implements hook_drush_command().
 */
function features_add_component_drush_command() {
  $items['features-add-component'] = array(
    'description' => 'Demonstrate how Drush commands work.',
    'arguments' => array(),
    'options' => array(),
    'drupal dependencies' => array('features'),
    'aliases' => array('fac'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function features_add_component_drush_help($section) {
  switch ($section) {
    case 'drush:features_add_component':
      return dt('Gives all users access devel information.');
  }
}

/**
 * Command function callback.
 */
function drush_features_add_component() {
  $component_type = _drush_features_add_component_choose_component_type();
  $component = _drush_features_add_component_choose_component($component_type);
  $feature = _drush_features_add_component_choose_feature();

  drush_features_export($feature, $component_type . ':' . $component);
}

/**
 * Helper, Choose a component type.
 */
function _drush_features_add_component_choose_component_type() {
  $component_types = array_keys(_drush_features_component_list());
  $choice = drush_choice($component_types, 'Choose a component type:');
  return $component_types[$choice];
}

/**
 * Helper, Choose a component.
 */
function _drush_features_add_component_choose_component($component_type) {
  $components = _drush_features_component_list();
  $component_options = array();
  $filtered_components = _drush_features_component_filter($components, array($component_type));
  foreach ($filtered_components['components'] as $source => $components) {
    foreach ($components as $name => $value) {
      $component_options[] = $name;
    }
  }

  $choice = drush_choice($component_options, 'Choose a component:');
  if ($choice === FALSE) {
    return;
  }

  return $component_options[$choice];
}

/**
 * Helper, Choose a feature.
 */
function _drush_features_add_component_choose_feature() {
  $features = array();

  foreach (features_get_features() as $name => $info) {
    $features[] = $name;
  }

  $choice = drush_choice($features, 'Choose a feature:');

  return $features[$choice];
}
